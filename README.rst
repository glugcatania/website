Generazione del sito
====================

(Italian only)

Preparazione
------------

- Installare pelican.
  Versione testata: 4.0.1 su Debian 10.

- Nella stessa directory, clonare:
  * https://gitlab.com/glucatania/pelican-vendored.git
  * https://gitlab.com/glucatania/website.git

- entrare nella directory `pelican-vendored` e seguire
  le istruzioni del file README.rst.
  Al momento dovrebbe essere sufficiente eseguire:
  
  ::

    make apply_patches

- dalla directory `website` eseguire:

  ::

    make html
  
  e il sito generato sarà accessibile della directory `output`.

Gestione dei contenuti
----------------------

Struttura
~~~~~~~~~

I contenuti sono posizionati nella directory `content` del repository
`website` (clone del sito). In particolare:

- gli articoli (con una data) sono direttamente all'interno di `content`.
  Per convenzione mantenere il nome progressivo `art_<num>`.
- le pagine statiche sono all'interno di `pages`.
  Una particolare eccezione sono le pagine in `pages/events`, i cui
  metadati sono usati per generare un calendario degli eventi
  in formato `.ics`.

Sia pagine che articoli possono includere dei metadati, delimitati da
`:...:`, che verranno usati nella generazione della pagina.

Alcuni metadati comunementi usati sono:

date
  La data completa in formato ISO

category
  Tra le più usate:
  - Vita sociale GLUG
  - Eventi
  - Guide
  - Sito
  - Varie
   wiki (per le pagine importate dal vecchio wiki)

tags
  Eventuali tag associati alla pagina. Tra i più usati:
  - GLUG: da usare per gli articoli
  - eventi,calendars: usati per le pagine degli eventi
    (che hanno anche altri metadati speciali)
  - contatti: per le pagine dei contatti

slug
  Lo slug (il nome della pagina nell'URL)

authors
  Semplice elenco degli autori. Il testo è in formato libero,
  ma bisogna mantenere i nomi coerenti, perché viene generata
  una pagina riassuntiva con tutti gli autori.

alias
  Altro indirizzo della pagina. Utile per mantenere attivo
  l'indirizzo che le pagine avevano nel sito precedente.

Si vedano gli articoli/pagine esistenti e la documentazione di Pelican
per maggiori informazioni.


Rigenerazione
~~~~~~~~~~~~~

Una volta inserito un nuovo articolo/pagina o modificato uno esistente,
per rigenerare i contenuti basta eseguire, dalla directory `website`:

::

  make clean
  make html

e il sito generato sarà accessibile in `output`.
