Pizze, pizzate & co...
======================

:date: 2007-01-20 17:40:53 +0100
:category: Vita sociale
:tags: GLUG
:slug: art_13_pizze_pizzate__co
:authors: Administrator
:alias: /content/view/13/1/

**AGGIORNAMENTO:** spostata la data del PizzaLinux di Scordia, ora il 4
febbraio, stesse modalità.

|
| Il sito non è aggiornatissimo, ma il GLUG è più che mai attivo, come
  si può vedere iscrivendosi alle mailing list.

Proseguono anche i tradizionali PPP del GLUGCT: come ogni ultimo giovedì
del mese (salvo qualche eccezione), anche oggi (**giovedi 25 gennaio**)
appuntamento in **piazza Verga alle 21.00** circa.

|
| Siamo anche lieti di riportare l’annuncio del PizzaLinux Day a
  Scordia! Se siete in zona, fate un salto.

 

::

    Il primo PizzaLinux di Scordia
    Quando: Giorno 31 Gennaio 4 Febbraio 2007,  ore 21:00
    Punto d'incontro: Piazza Roma (di fronte al Krossower)
    Dove: A scelta tra "A casa russa", "a vanedda" e "le 4 stagioni",
    poi si decide tutti insieme
    Perché: Perché i pinguini hanno fame
    Cosa devo portare?: I soldi per la pizza, e possibilmente il portatile
    Dopocena: dopo cena ne parliamo!
    Altre info: contattateci a scordialinux[chiocciola]altervista[punto]org

|
| p.s.: date la vostra disponibilità inviando una mail a
  scordialinux[chiocciola]altervista[dot]org
