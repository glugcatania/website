GNU/Linux Day 2006 @ Catania
============================

:date: 2006-11-15 17:52:26 +0100
:category: Eventi
:tags: GLUG
:slug: art_14_gnulinux_day_2006__catania
:authors: Administrator
:alias: /content/view/14/1/

| GNU/Linux User Group Catania, Freaknet Medialab e ASIC, organizzano il
  **GNU/Linux Day**, giornata dedicata alla divulgazione della cultura
  del Software Libero e delle applicazioni e dei sistemi operativi
  liberi, in primo luogo quelli basati sul kernel Linux.
| La manifestazione si terrà nei giorni **24 e 25 novembre 2006** .

Programma (suscettibile di alcune variazioni):

|
| **24 nov, ore 16:00-19:00: ARCI Catania, via Landolina 41**
| (da stabilire)

|
| **25 nov, ore 9:00-13:00: Aula Magna, Facoltà di Ingegneria,
  Cittadella Universitaria**
| *Prima parte: GNU/Linux e il software libero*

-  Introduzione al software libero
-  Perchè il Software Libero è divertente
-  (interfacce grafiche)
-  Multimedia e GNU/Linux
-  Usabilità del Software Libero

| *Seconda parte: Cultura digitale*

-  I pericoli di DRM/TCPA
-  (Hacking)
-  Il sapere liberato

*Question time*

|
| Sarà disponibile un’apposita area dimostrativa di postazioni
  specializzate (Internet, produttività personale, giochi, multimedia,
  programmazione, etc…)

|
| **25 nov, ore 16:00-19:00 *ARCI Catania*, via Landolina 41**
| Installation party, gaming on LAN e altro

| **25 nov, ore 16:00-18:00 *Laboratorio Multimediale, Liceo Scientifico “Galileo Galilei” (http://www.liceoscientificogalilei.catania.it)**
  ** Junior Session: Software educativo per la didattica **
|  (sessione per docenti e studenti a numero limitato di posti,
  rivolgersi a edu@catania.linux.it) 

Per maggiori informazioni, e per aggiornamenti sul programma, vedere

-  http://catania.linux.it
-  http://freaknet.org
-  http://asiconline.org
