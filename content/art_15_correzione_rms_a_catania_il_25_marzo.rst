Correzione !!!! RMS a Catania il 25 Marzo !!!
=============================================

:date: 2006-03-20 11:00:00 +0100
:category: Eventi
:tags: GLUG
:slug: art_15_correzione_rms_a_catania_il_25_marzo
:authors: Administrator
:alias: /content/view/15/1/

ATTENZIONE!!!! ATTENZIONE!!!! ATTENZIONE!!!!
QUESTA NON E’ UNA ESERCITAZIONE !!!
La data dei seminari di Richard Stallman e’ stata SPOSTATA, per motivi
logistici, **DAL 27 MARZO AL 25 MARZO**.
RIPETO: RICHARD STALLMAN terra’ due seminari all’Aula Magna di
Ingegneria SABATO 25 MARZO E NON LUNEDI’ 27 MARZO.
SABATO 25 MARZO, ORE 9:00
SABATO VENTICINQUE MARZO ALLE ORE NOVE.

::

     ____    _    ____    _  _____ ___
    / ___|  / \  | __ )  / \|_   _/ _ \
    \___ \ / _ \ |  _ \ / _ \ | || | | |
     ___) / ___ \| |_) / ___ \| || |_| |
    |____/_/   \_\____/_/   \_\_| \___/

    __     _______ _   _ _____ ___ ____ ___ _   _  ___  _   _ _____
    \ \   / / ____| \ | |_   _|_ _/ ___|_ _| \ | |/ _ \| | | | ____|
     \ \ / /|  _| |  \| | | |  | | |    | ||  \| | | | | | | |  _|
      \ V / | |___| |\  | | |  | | |___ | || |\  | |_| | |_| | |___
       \_/  |_____|_| \_| |_| |___\____|___|_| \_|\__\_\\___/|_____|

     __  __    _    ____   ________
     |  \/  |  / \  |  _ \ |__  / _ \
     | |\/| | / _ \ | |_) |  / / | | |
     | |  | |/ ___ \|  _ <  / /| |_| |
     |_|  |_/_/   \_\_| \_\/____\___/


      ___  ____  _____    ___         ___   ___
     / _ \|  _ \| ____|  / _ \   _   / _ \ / _ \
    | | | | |_) |  _|   | (_) | (_) | | | | | | |
    | |_| |  _ <| |___   \__, |  _  | |_| | |_| |
     \___/|_| \_\_____|    /_/  (_)  \___/ \___/


    RIPETO: NON E' UNO SCHERZO!!!! NON E' UNA ESERCITAZIONE!!!!
    DIFFONDETE IL MESSAGGIO A

     _____ _   _ _____ _____ ___
    |_   _| | | |_   _|_   _|_ _|
      | | | | | | | |   | |  | |
      | | | |_| | | |   | |  | |
      |_|  \___/  |_|   |_| |___|
