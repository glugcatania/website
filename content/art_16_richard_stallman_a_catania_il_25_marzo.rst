Richard Stallman a Catania il 25 Marzo
======================================

:date: 2006-03-06 00:00:00 +0100
:category: Eventi
:tags: GLUG
:slug: art_16_richard_stallman_a_catania_il_25_marzo
:authors: Administrator
:alias: /content/view/16/1/

 Comunicato Stampa
------------------

|  
| Sabato 25 Marzo 2006 Richard Stallman sarà a Catania per un seminario
  sul progetto GNU e sul pericolo dei brevetti software. L’evento,
  organizzato da LUGCT – GNU/Linux User Group Catania in collaborazione
  con Freaknet, storico hacklab siciliano, si terra’ presso l’Aula Magna
  della Facolta’ di Ingegneria dell’Universita’ di Catania. Stallman,
  iniziatore del progetto GNU e padre fondatore della filosofia del
  Software Libero, terra’ un seminario sulla storia del progetto GNU e
  dell’apporto della comunita’ GNU allo sviluppo del sistema operativo
  GNU/Linux. Nel pomeriggio e’ previsto un seminario sui pericoli
  connessi alla brevettabilita’ del software e degli algoritmi,
  argomento molto sentito dalla comunita’ del Software Libero, preceduto
  da un breve intervento a cura di LUGCT e Freaknet sullo stato attuale
  dei “diritti digitali in Italia”.
|

 Il programma della giornata e’ il seguente:
 
 9:30 : Introduzione (Prof. Luigi Fortuna, preside della Facolta’ di
Ingegneria)
 10:00 : The Free Software Movement and the GNU/Linux operating system
(R. Stallman)
 12:00 : Q&A session
 12:30 : Pranzo
 
 16:00 : I diritti digitali in Italia: anno zero (LUGCT – Freaknet)
 16:30 : The danger of software patents(R. Stallman)
 18:15 : Q&A session
 19:00 : Ringraziamenti e chiusura dei lavori
 
 
 Link:
 
 http://catania.linux.it
 http://www.freaknet.org
