Linux Day 2007
==============

:date: 2007-10-10 17:52:09 +0200
:category: Eventi
:tags: GLUG
:slug: art_21_linux_day_2007
:authors: Emanuele
:alias: /content/view/21/1/

GNU/Linux User Group Catania organizza il Linux Day, giornata dedicata
alla divulgazione della cultura del Software Libero e delle applicazioni
e dei sistemi operativi liberi, in primo luogo quelli basati sul kernel
Linux.  La manifestazione si terrà giorno **27 Ottobre** a partire dalle
ore **9.00** a **Catania** presso la **Cittadella Universitaria,** viale
A.Doria n.6

.. image:: /images/manifesto_ld_ct_small.png
   :alt: Poster LinuxDay 2007 Catania

|  Anche quest’anno il “GNU/Linux User Group Catania” organizza il Linux
  Day ( http://www.linuxday.it ), giornata nazionale organizzata dalla
  Italian Linux Society e dedicata alla divulgazione della cultura del
  Software Libero e delle applicazioni e dei sistemi operativi liberi,
  in primo luogo quelli basati sul kernel Linux.
| Come nel resto d’Italia, la manifestazione si terrà **giorno 27
  Ottobre**.

A Catania sarà divisa in due sessioni:

Una prima sessione si terrà in mattinata presso la cittadella
universitaria e verterà sul **software libero in generale.**

Una seconda sessione si terrà nel pomeriggio presso l’istituto “Regina
Elena” di Acireale, e verterà sull’uso specifico del **software libero
nella didattica.**

 

| Segue un programma di massima, soggetto a probabili cambiamenti (che
  verranno notificati su questo stesso sito).
| **ore 9:00-13:00: Cittadella Universitaria**, Aula Magna Facoltà di
  Ingegneria

    -  Introduzione
    -  presentazione su software libero
    -  Le licenze del software libero
    -  Software per la didattica
    -  Usare nella vita reale i sistemi liberi è possibile
    -  Grafica, animazione e multimedialita’
    -  Anonimato, privacy e i pericoli del software chiuso
    -  Question Time

|
| Come nella nostra tradizione, ampio spazio verrà dedicato alle
  **domande** del pubblico.
| Se lo si desidera, queste potranno anche essere presentate in forma
  scritta ed anonima, prima, durante e dopo le presentazioni. Cercheremo
  di rispondere alle domande più interessanti.
| Sarà disponibile un’apposita **area dimostrativa** dove potrete
  ‘toccare con mano’ varie applicazioni di uso quotidiano, lavorativo,
  ricreativo (Internet, produttività personale, giochi, multimedia,
  programmazione, etc…)
| Inoltre, a coloro che porteranno un supporto ottico vergine (CD-R,
  DVD-R,+R etc) verrà gratuitamente **masterizzata** una distribuzione
  di GNU/Linux. Con il software libero non solo questo è possibile, ma è
  perfettamente legale!
| **ore 16**-: Istituto superiore **“Regina Elena”, Acireale**

    -  Software per la didattica  (presentazione più ampia)
    -  Question Time e dimostrazioni pratiche

|
| A tutti auguriamo un buon Linux Day!
