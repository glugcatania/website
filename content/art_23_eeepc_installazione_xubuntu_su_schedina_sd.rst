eeepc: installazione Xubuntu su schedina SD
===========================================

:date: 2008-03-14 05:48:10 +0100
:category: Guide
:tags: Documentazione,hidden
:slug: art_23_eeepc_installazione_xubuntu_su_schedina_sd
:authors: Giorgio Beretta
:alias: /content/view/23/1/ /content/view/23/44/

HOWTO: installazione su eeepc di Xubuntu su schedina SD (senza
interferire con Xandros)

.. image:: /images/xubuntu.jpg
   :alt: foto eepc
 

 

In `questo HOWTO </~stragatto/eeepc/eeeXubuntu_HOWTO.html>`__ descrivo
le fasi della mia (perfettamente riuscita) installazione di eeeXubuntu
su scheda esterna SD HC da 4 GB
