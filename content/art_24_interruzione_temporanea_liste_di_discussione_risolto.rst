Interruzione temporanea liste di discussione [risolto]
======================================================

:date: 2008-04-15 03:44:41 +0200
:category: Sito
:tags: GLUG
:slug: art_24_interruzione_temporanea_liste_di_discussione_risolto
:authors: Luigi Toscano
:alias: /content/view/24/1/

Un piccolo avviso di servizio: se siete iscritti alle `liste di
discussione </index.php?option=com_content&task=view&id=12&Itemid=30>`__
del GLUG avrete notato sicuramente che sono fuori servizio da domenica.

Niente panico!
--------------

Il server che ospita le liste, di cui era giÃ  prevista la chiusura con
conseguente migrazione, è stato bloccato in anticipo. I lavori per la
migrazione erano fortunatamente a buon punto, il nuovo server è
praticamente pronto. Stiamo aspettando che i server DNS si aggiornino
(già oggi, giorno 15/04, *potrebbe* tornare tutto in linea).

Aggiornamento: fortunatamente nei tempi previsti le liste sono tornate attive!
------------------------------------------------------------------------------
