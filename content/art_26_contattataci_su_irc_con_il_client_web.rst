Contattataci su IRC con il client web!
======================================

:date: 2008-04-15 16:42:21 +0200
:category: Sito
:tags: GLUG
:slug: art_26_contattataci_su_irc_con_il_client_web
:authors: netlex
:alias: /content/view/26/1/

A partire da oggi chi volesse contattarci via IRC e non ha la possibiltà
di installare un client IRC potrà utilizzare l’apposita interfaccia web
reperibile all’indirizzo http://catania.linux.it/cgiirc/

L’utilizzo è semplice ed intuitivo: basta inserire il proprio nick e
fare clic su LOGIN. Attenzione: utilizzando il client web i dati
viaggiano in chiaro.

Se ne avete la possibilità è preferibile utilizzare un client
tradizionale (ad es. xchat, konversation, pidgin ecc), le impostazioni
da utulizzare sono le seguenti: server catania.linux.it porta 9999
connessione protetta da SSL. Se ci si connette via SSL occorrerà
istruire il client affichè accetti certificati invalidi/autofirmati.
