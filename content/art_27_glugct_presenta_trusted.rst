GLUGCT presenta: Trusted??
==========================

:date: 2008-07-08 12:00:00 +0200
:category: Sito
:tags: GLUG
:slug: art_27_glugct_presenta_trusted
:authors: Luigi Toscano
:alias: /content/view/27/1/

Ci siamo! Il piccolo video introduttivo su Trusted Computing e Software
Libero prodotto dal GNU/Linux User Group Catania in occasione del
LinuxDay 2007 è finalmente disponibile! **Aggiornamento: è ora anche
possibile scaricare il video!**

!youtube(_Q0kASug9Qc)

(licenza:
`CC-BY-SA <http://creativecommons.org/licenses/by-sa/3.0/>`__).

Il video è stato realizzato interamente con software libero.

Il video è disponibile in formato OGG/Theora presso i seguenti mirror:

-  http://www.freaknet.org/mirror/trusted.ogg (inglese:
   http://www.freaknet.org/mirror/trusted-eng.ogg)
-  http://galileo.dmi.unict.it/~toscano/trusted.ogg (inglese:
   http://galileo.dmi.unict.it/~toscano/trusted-eng.ogg)

Potete anche ammirare(?) il
`backstage <http://www.flickr.com/photos/ivan_iraci/sets/72157602426234456/>`__
(grazie alle foto di Ivan!)
