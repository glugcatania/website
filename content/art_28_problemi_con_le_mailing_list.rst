Problemi con le mailing list
============================

:date: 2008-06-14 12:03:51 +0200
:category: Varie
:tags: GLUG
:slug: art_28_problemi_con_le_mailing_list
:authors: Mancausoft
:alias: /content/view/28/1/

A causa della “mania” della lotta allo spam di alcuni provider potreste
non ricevere le email dalla mailinglist. Al momento siamo blacklistati
da server mail di: tiscali, tin.it, libero (e derivati), Hotmail.

Ci scusiamo per il disagio, stiamo cercando di fare il possibile per
ristabilire il normale funzionamento delle liste.

**Aggiornamento**  14-06-2008 18:01:43: La situazione è migliorata.
Tiscali, tin.it e hotmail ricevano i messaggi della lista, solo libero
ancora si rifiuta di riceverli. 

**Aggiornamento**  15-06-2008 00:22:56: Allarme rientrato. Al momento
non dovrebbero esserci più problemi. Finalmente anche libero accetta le
email della mailing list. 

**Aggiornamento**  17-06-2008 09:03:36: Siamo stati inseriti di nuovo
nella lista di spamcop a causa del fatto che se scrive un non iscritto
alla lista viene mandato un messaggio automatico all’utente che lo
informa che il messaggio è stato bloccato.
http://www.spamcop.net/fom-serve/cache/329.html#bounceexplain

 **Aggiornamento**  17-06-2008 14:04:19: Al momento le liste funzionano
correttamente e non siamo piu’ in blacklist.
