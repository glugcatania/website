Archeologia post-industriale
============================

:date: 2008-08-23 16:13:34 +0200
:category: Eventi
:tags: GLUG
:slug: art_29_archeologia_postindustriale
:authors: Luigi Toscano
:alias: /content/view/29/1/

Riceviamo e diffondiamo con piacere:

--------------

-  Freaknet MediaLab, Catania - http://freaknet.org
-  Poetry Hacklab, Palazzolo Acreide - http://poetry.freaknet.org
-  Dyne.org foundation, Amsterdam - http://dyne.org
-  Museo dell’Informatica Funzionante - http://museo.freaknet.org

PRESENTANO:

ARCHEOLOGIA POST-INDUSTRIALE
----------------------------

| Esposizione interattiva di computer storici funzionanti, software,
  accessori, documentazione, calcolatrici antiche
| Nel corso della manifestazione: Installazioni di GNU/Linux, Dibattiti,
  Proiezioni, Videogames, e la possibilita’ di usare con le proprie mani
  computer storici risalenti persino agli anni ’70.

.. image:: /images/449px-expo2831082008.jpg
   :alt: Manifesto

| Dal 28 al 31 Agosto 2008
| dalle 11:00 alle 12:30 e dalle 16:00 alle 22:00,
| Palazzolo Acreide (SR), Corso Vittorio Emanuele 4A

Le nostre vite oggi sono condizionate dall’uso di apparecchiature
informatiche sempre piu’ piccole, potenti, versatili: telefoni
cellulari, palmari, computer portatili diventano indispensabili e
necessari, semplificano le comunicazioni, favoriscono le interazioni
sociali, stimolano la creativita’ e permettono di scambiare enormi
quantita’ di dati ed informazioni. Le reti informatiche oggi
costituiscono architetture nelle quali viviamo, spazi pubblici di grande
importanza per il nostro sviluppo intellettuale e sociale.
Pochi anni fa, tutto cio’ era semplicemente impensabile: le risorse
informatiche erano appannaggio di pochi eletti in camice bianco,
scienziati che muovevano i loro passi attorno a calcolatori grandi,
pesanti, costosissimi ed il cui accesso era solitamente proibito ai
comuni mortali. Venivano usati da grosse istituzioni e centri di
ricerca.
Con l’evoluzione i computer sono diventati un passatempo per
appassionati ed amatori, poi strumento di gioco e di svago per bambini e
ragazzi, infine oggetti di uso quotidiano accessibili a tutti.
Architetture dell’informazione sulle quali viaggiano i nostri pensieri,
le nostre sensazioni, le nostre relazioni umane.
Ma dove e’ scritta la storia del progresso dell’informatica? Chi furono
i veri protagonisti, e come erano fatti i computer di una volta? Ma
soprattutto, come funzionavano? Come venivano utilizzati? Quali software
ci giravano sopra? Che sistemi operativi utilizzavano? Quali linguaggi
di programmazione erano disponibili? Quali sono i paradigmi secondo i
quali si sono sviluppati? qual’e’ la storia delle citta’ digitali che
oggi i giovani di tutto il mondo popolano, delle piazze virtuali che
ospitano le nostre interazioni?
Abbiamo voglia di raccontarvi almeno una parte di questa storia
affascinante, facendovi vedere gli stessi protagonisti: i computer, i
programmi, i sistemi operativi!
Ed abbiamo scelto di svolgere questo racconto in una maniera particolare
e “viva”: non un freddo museo con opere “morte” ed intoccabili, esposte
dietro una vetrina, bensi’ creando un vero museo interattivo che
raccogliesse computer storici funzionanti, che possano essere accesi,
provati, ed utilizzati dai visitatori, con i loro sistemi operativi
originali, i programmi di un tempo, sui supporti, dischi, nastri, schede
di allora.
Perche’ crediamo che il miglior modo per raccontare una storia sia
quello di riviverla, e di farla rivivere a chi non l’ha vissuta, e cosi’
imparare anche dal passato, per riflettere su un futuro che e’ sempre
piu’ veloce e che rischia di dimenticare le sue origini.
Per questo motivo abbiamo creato il “Museo dell’Informatica
Funzionante”, che oggi presenta a voi questa esposizione. Partecipate
numerosi!

Freaknet Medialab / Poetry Hacklab / Dyne.org
http://museo.freaknet.org
museo@freaknet.org
