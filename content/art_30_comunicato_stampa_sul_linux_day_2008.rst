Comunicato stampa sul Linux Day 2008
====================================

:date: 2008-10-18 21:44:43 +0200
:category: Eventi
:tags: GLUG
:slug: art_30_comunicato_stampa_sul_linux_day_2008
:authors: Daniele Tricoli
:alias: /content/view/30/1/

Il Catania GNU/Linux User Group, unitamente al `Poetry
Hacklab <http://poetry.freaknet.org/>`__ e al `Freaknet
Medialab <http://freaknet.org/>`__, rende noto che quest’anno non
parteciperà al Linux Day coordinato da ILS.
La decisione, in realtà già maturata da alcuni anni, deriva dalla
differenza di vedute poste al centro dell’organizzazione di una
manifestazione che è volta alla divulgazione del Software Libero.

Nulla togliendo ai meriti della visibilità mediatica portata dal Linux
Day nel corso di questi anni, ci siamo però trovati in disaccordo con le
dinamiche organizzative e comunicative e con i messaggi che tramite
questi vengono veicolati ai fruitori del Linux Day da parte di alcune
associazioni che vi aderiscono. Prima tra tutte, l’aver posto al centro
della manifestazione gli aspetti tecnologici e di “marketing” al fine di
ottenere la massima eco.

Noi siamo fermamente convinti che non può essere tecnologica la leva che
deve spostare la gente verso il software libero, ma deve essere etica e
culturale. Se infatti le tecnologie proprietarie dovessero sorpassare da
un punto di vista meramente tecnico quelle libere, la stessa gente che
avesse effettuato la transizione verso queste ultime, tornerebbe a
scegliere la propria prigione dorata. Invece, se l’accento è posto con
estrema chiarezza sugli aspetti etici, culturali e filosofici, si mette
in guardia l’uditorio, in maniera non ambigua, sulla possibilità di
scegliere consapevolmente tra delle soluzioni proprietarie che, pur
apparendo a basso costo (o nullo, grazie alla pirateria informatica) e
tecnologicamente valide, costituiscono un argine al progresso della
conoscenza e delle soluzioni che garantiscono delle libertà che 
dovrebbero essere inalienabili alla Persona, anche se il costo di queste
Libertà (come in tutti i campi, del resto) potrebbero essere nel breve o
medio periodo dei piccoli sacrifici o delle condizioni di lieve disagio
rispetto a chi sceglie la “cella dorata” delle soluzioni proprietarie.
Da questa scelta di Libertà discendono indiscussi vantaggi tecnologici
che vanno evidenziati ma che non vanno confusi col reale e sostanziale
fulcro della battaglia per il Software Libero: la restituzione della
libertà di evoluzione ai prodotti dell’ingegno, in particolar modo in
ambito informatico.

|
| In tal senso, non possiamo accettare che una manifestazione così
  importante possa essere asservita a logiche di marketing da parte di
  quanti la organizzano, perché non si può non guardare con sospetto
  chi, per fare business, da una parte si fa paladino del Software
  Libero ma dall’altra fa ampio ricorso ai brevetti sui prodotti
  dell’ingegno, limitando di fatto le possibilità di diffusione dello
  stesso Software Libero. Né accettiamo che gli obiettivi della
  manifestazione, per come vengono da noi percepiti e proposti al nostro
  uditorio da anni, possano essere conciliabili con vetrinette
  espositive per l’imprenditoria più o meno locale, in alcuni casi
  neanche produttrice di software che ricada sotto la categoria del
  Software Libero. Se si volesse creare una manifestazione locale o
  nazionale che risalti i benefici del Software Libero per il business,
  fornendo un vero pluralismo di informazioni e la possibilità a tutte
  le realtà del settore di esporsi, sarebbe ben venuta.
| Ma che non sia una fiera delle software house o delle ultime
  tecnologie che tenta di usurpare il nome di una giornata che doveva
  essere dedicata invece al Software Libero e alla Cultura della
  Partecipazione.

Maggiori informazioni sulla giornata che si svolgerà a Catania verranno
divulgate prossimamente.
