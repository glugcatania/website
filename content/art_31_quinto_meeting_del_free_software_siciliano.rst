Quinto meeting del Free Software Siciliano
==========================================

:date: 2009-03-21 14:34:50 +0100
:category: Eventi
:tags: GLUG
:slug: art_31_quinto_meeting_del_free_software_siciliano
:authors: Luigi Toscano
:alias: /content/view/31/1/

Si chiama *Linux Joint day* e si terra’ Sabato 28 e Domenica 29 a
Villarosa in provincia di Enna, il quinto meeting ufficiale dei pinguini
siciliani in una “due giorni” caratterizzata da incontri, dibattiti e
seminari sull’utilizzo delle tecnologie libere, sul free software e
sull’indipendenza della Rete.

Oltre ai Linux User Group dell’isola, aderiranno all’evento in uno
spirito di condivisione e apprendimento collettivo anche altre
associazioni, movimenti e controculture digitali made in sicily,
l’associazione VerdeBinario di Cosenza, nonche’ i principali hacklab
della regione.

La partecipazione, in pieno spirito “free”, rimane comunque aperta ad
appassionati o curiosi non appartenenti a nessun gruppo, ma che vogliano
comunque conoscere e sperimentare il sistema operativo GNU/Linux e tutte
le realta’ ad esso collegate.

Durante l’incontro verranno affrontate e discusse varie tematiche,
principalmente sul sistema operativo GNU/Linux.

Per raggiungere il posto:
http://www.sangiovannello.it/it/dove-siamo.html

| Per tutte le altre informazioni contattare:
| geenna at freaknet.org
| me at mancausoft.org
| scinziatu at freaknet.org

Venite Numerosi :-)
