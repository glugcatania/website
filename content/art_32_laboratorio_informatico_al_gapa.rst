Laboratorio informatico al GAPA
===============================

:date: 2009-10-20 14:14:06 +0200
:category: Eventi
:tags: GLUG
:slug: art_32_laboratorio_informatico_al_gapa
:authors: Emanuele
:alias: /content/view/32/1/

È in funzione al `centro di quartiere
GAPA <http://www.associazionegapa.org/>`__ in via Cordai il laboratorio
informatico allestito a cura del Gnu/Linux User Group di Catania. Il
laboratorio comprende 6 computer che erano stati dismessi perché
ritenuti obsoleti dai precedenti proprietari e riportati alla piena
efficienza tramite piccoli interventi di manutenzione e installazione di
Ubuntu. Il laboratorio è utilizzato dal GAPA, con l’assistenza del GLUG
Catania,  per corsi gratuiti di informatica di base e office
applications.
