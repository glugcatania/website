Comunicato stampa sgombero CPO Experia, Catania
===============================================

:date: 2009-10-31 20:02:03 +0100
:category: Varie
:tags: GLUG
:slug: art_34_comunicato_stampa_sgombero_cpo_experia_catania
:authors: Mancausoft
:alias: /content/view/34/1/

Con la tecnica del carciofo, dopo Piazza Europa, dopo l’edificio
scolastico di via De Nicola, poco per volta, pezzo per pezzo, Catania
continua a venire sottratta ai suoi abitanti. Ora è toccato al Centro
Experia. Quello che in termini di servizio pubblico era stato costruito
dai cittadini per i cittadini in 17 anni, è stato espropriato in poche
ore. Ancora una volta è stato dimostrato che è più (rozzamente) facile
distruggere che costruire. Certo, si potrà obiettare che l’edificio,
proprietà della Provincia, era stato occupato (17 anni fa!)
abusivamente. Ma oltre alle proprietà materiali esistono anche quelle
immateriali, e anche queste hanno un valore che - volendo - si potrebbe
provare  quantizzare. Qual’è il valore di 17 anni di doposcuola per i
bambini del quartiere? Quale è il valore dei tanti corsi e delle tante
attività che sono serviti a tenerli lontani dalla strada e dalla droga?
Quanto avrebbero dovuto spendere il Comune e la Provincia in 17 anni se
avessero sentito il dovere di offrire queste cose alla cittadinanza ?
OK, domanda oziosa, perché non l’hanno sentito, ma è comunque un valore
quantificabile. E allora, possiamo trattare da abusivi i cittadini che
di fronte a una necessità hanno provveduto come hanno potuto, e hanno
anche provveduto bene ? E, stabilito che i servizi offerti avevano un
valore (e probabilmente superiore a quello dell’edificio stesso), era
davvero il caso di buttarlo via in questo modo ?
