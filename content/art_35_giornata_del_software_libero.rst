Giornata del Software Libero
============================

:date: 2009-11-22 13:21:11 +0100
:category: Eventi
:tags: GLUG
:slug: art_35_giornata_del_software_libero
:authors: Ivan Iraci
:alias: /content/view/35/1/

| Il 28 novembre avrà luogo congiuntamente a Catania e a Ragusa la
  **“Giornata del Software Libero”**.
| |Manifesto della Giornata del Software Libero 2009|

La manifestazione, consistente in una serie di brevi seminari, è
patrocinata a Catania dal Dipartimento di Matematica e Informatica
(`SDAM <http://www.sdam.unict.it/>`__ e
`SDAI <http://www.sdai.unict.it/>`__ ) e a Ragusa dal Comune di Ragusa.

 

| I seminari in programma avranno luogo in mattinata a Catania dalle ore
  09:30 nell’Aula Magna del Dipartimento di Matematica e Informatica
  alla Cittadella Universitaria di Viale Andrea Doria e nel pomeriggio a
  Ragusa dalle 16:30 nell’Aula Consiliare del Palazzo dell’Aquila in
  Corso Italia 72.

 

Le due sessioni saranno curate dai LUG locali (GLUG-Catania e
Solira-Ragusa) con la partecipazione del Freaknet Medialab.

.. |Manifesto della Giornata del Software Libero 2009| image:: /images/sl2009.png
   :width: 250px
   :height: 354px
