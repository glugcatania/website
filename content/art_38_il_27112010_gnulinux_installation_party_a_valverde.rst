Il 27/11/2010, GNU/Linux Installation Party a Valverde
======================================================

:date: 2010-11-08 17:00:13 +0100
:category: Eventi
:tags: GLUG
:slug: art_38_il_27112010_gnulinux_installation_party_a_valverde
:authors: Ivan Iraci
:alias: /content/view/38/1/

|Locandina dello GNU/Linux installation party del 27/11/2010 a Valverde|

I soci del *GNU/Linux User Group di Catania* saranno lieti di incontrare
neo utenti GNU/Linux, simpatizzanti o semplici curiosi che vogliano
provare o abbiano bisogno di consigli sull’installazione di Sistemi
Operativi liberi o sul Software Libero in genere.

 

Vi aspettiamo numerosi dalle **ore 15:30** (ora Italiana) del **27
Novembre 2010** in **Via Eremo S. Anna 41 a Valverde (CT)**.

 

**Percorso**: Dalla piazza principale di Valverde andare fino in fondo
al Corso V.Emanuele e svoltare a sinistra per via del Santuario. Seguire
le indicazioni stradali per Eremo S.Anna. 

.. |Locandina dello GNU/Linux installation party del 27/11/2010 a Valverde| image:: /images/locandina-definitiva.png
   :width: 362px
   :height: 512px
