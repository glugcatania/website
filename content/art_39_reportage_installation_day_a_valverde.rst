Reportage Installation Day a Valverde
=====================================

:date: 2010-11-30 10:48:08 +0100
:category: Eventi
:tags: GLUG
:slug: art_39_reportage_installation_day_a_valverde
:authors: pietro
:alias: /content/view/39/1/

Sabato 27 Novembre si è tenuta un pomeriggio di installazione di linux e
aiuto per gli utenti che si approcciano per la prima volta al mondo
dell’open source, anche l’aiuto a chi come me, magari usa software
libero da anni ma non ha mai programmato e si trovava a sbattere la
testolina sul tavolo per far funzionare quattro righe di codice che
aveva scritto. 

La giornata si è svolta con una riunione del GLug la mattina,  nella
bellissima sede messaci a disposizione da Mirthrandir e dalla sua
associazione, vicino all’eremo di S. Anna a  Valverde. Dopo una vivace
discussione, seguita anche dal buon Tosky che tra le spalate di neve dal
balcone ci seguiva ed interveniva attraverso la rete, siamo giunti
all’ora di pranzo. Qui le doti culinarie di Sarik si sono espresse in
una buona pasta con la salsa e pecorino per imbiancare il rosso della
salsa.

Nel pomeriggio ci siamo dedicati ai pc, aiutando chi ne avesse bisogno,
sia tecnicamente che mentalmente, cioè abbiamo dato qualche spiegazione
di base e qualche dritta su dove trovare tutte le info necessarie per
imparare da se stessi di più su linux e l’opensource.

Sabato 27 Novembre si è tenuta un pomeriggio di installazione di linux e
aiuto per gli utenti che si approcciano per la prima volta al mondo
dell’open source, anche l’aiuto a chi come me, magari usa software
libero da anni ma non ha mai programmato e si trovava a sbattere la
testolina sul tavolo per far funzionare quattro righe di codice che
aveva scritto. 

La giornata si è svolta con una riunione del GLug la mattina,  nella
bellissima sede messaci a disposizione da Mirthrandir e dalla sua
associazione, vicino all’eremo di S. Anna a  Valverde. Dopo una vivace
discussione, seguita anche dal buon Tosky che tra le spalate di neve dal
balcone ci seguiva ed interveniva attraverso la rete, siamo giunti
all’ora di pranzo. Qui le doti culinarie di Sarik si sono espresse in
una buona pasta con la salsa e pecorino per imbiancare il rosso della
salsa.

Nel pomeriggio ci siamo dedicati ai pc, aiutando chi ne avesse bisogno,
sia tecnicamente che mentalmente, cioè abbiamo dato qualche spiegazione
di base e qualche dritta su dove trovare tutte le info necessarie per
imparare da se stessi di più su linux e l’opensource.

Ora una serie di foto.

 

|Ingresso|

 L’ingresso 

 

| | Quattro chiacchere fuori|
| Quattro chiacchiere fuori

|nignux, Disgustato o solo la posa criptica?| 

Nignux, disgustato o la sua espressione da marpione?

 |Scene dall'interno| 

Dentro tra pc e chiacchere.

 

|Giù coi pc| 

Giù con le mani sulle tatiere! 

|Angolo cottura| 

L’angolo cottura, l’ora del pranzo si avvicina. 

|Sarik all'opera|

 The great chef, Sarìk (la ì accentata fa più cuoco francese)

|Il porta bicchiere :D| 

Un poggia bicchiere. 

 |L'opera d'arte culinaria!|

 L’opera culinaria, gnam :D

|Si mangia!!!| 

 Dopo giù le mani sulle tastiere, giù le forchette sulla pasta

|Vai di cacciavite!| 

Vai di cacciavite! 

|Avanza un pezzo, dove va messo??|

Mmmm, ma questo pezzo dove andrà? 

|Nel pieno dell'attività pomeridiana| 

Nel pieno dell’attività pomeridiana. 

|Chissà cosa faranno?| 

Tutti a guardare… ma cosa? 

|Un po' di teoria, non fa mai male.| 

Qualche spiegazione su come e perchè. 

|Tipica scena italiana, uno lavora e gli altri guardano :D| 

Scena già vista, uno lavora e gli altri… uniscono i cervelli per
aiutarlo, forse…  

|Tutti al riparo... potrebbe esplodere!| 

Tutti al riparooooo…..  

 

.. |Ingresso| image:: /images/foto/01installparty.jpg
.. | Quattro chiacchere fuori| image:: /images/foto/03discuss.jpg
   :width: 1000px
   :height: 678px
.. |nignux, Disgustato o solo la posa criptica?| image:: /images/foto/04nignux.jpg
.. |Scene dall'interno| image:: /images/foto/DSC_3174.JPG
.. |Giù coi pc| image:: /images/foto/DSC_3176.JPG
.. |Angolo cottura| image:: /images/foto/DSC_3178.JPG
.. |Sarik all'opera| image:: /images/foto/DSC_3179.JPG
.. |Il porta bicchiere :D| image:: /images/foto/DSC_3185.JPG
   :width: 912px
   :height: 606px
.. |L'opera d'arte culinaria!| image:: /images/foto/DSC_3192.JPG
.. |Si mangia!!!| image:: /images/foto/DSC_3186.JPG
.. |Vai di cacciavite!| image:: /images/foto/DSC_3211.JPG
.. |Avanza un pezzo, dove va messo??| image:: /images/foto/DSC_3215.JPG
.. |Nel pieno dell'attività pomeridiana| image:: /images/foto/DSC_3221.JPG
.. |Chissà cosa faranno?| image:: /images/foto/DSC_3222.JPG
.. |Un po' di teoria, non fa mai male.| image:: /images/foto/DSC_3223.JPG
.. |Tipica scena italiana, uno lavora e gli altri guardano :D| image:: /images/foto/DSC_3225.JPG
.. |Tutti al riparo... potrebbe esplodere!| image:: /images/foto/DSC_3226.JPG
