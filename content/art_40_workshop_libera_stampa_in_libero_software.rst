WORKSHOP/ Libera stampa in libero software
==========================================

:date: 2011-03-06 09:15:33 +0100
:category: Eventi
:tags: GLUG
:slug: art_40_workshop_libera_stampa_in_libero_software
:authors: Ivan Iraci
:alias: /content/view/40/1/

Due giorni di lavori al Gapa
per trattare i temi relativi all’utilizzo di un’informatica libera per
chi vuole fare informazione dal basso.

 

Gli incontri, in programma il 12 e il 13 marzo, consisteranno in lezioni
ed esercitazioni sull’uso di programmi di videoscrittura, video
impaginazione, elaborazione delle immagini, grafica vettoriale e
creazione di siti web.

 

Per iscriversi occorre inviare entro il 7 marzo la scheda di adesione,
che trovi allegata all’articolo, all’indirizzo
redazione.lavoriincorso@gmail.com

 


.. rubric:: 12-13 MARZO 2011
   :name: marzo-2011
   :class: spip

Al G.A.P.A. – via Cordai 47, Catania

 

.. rubric:: WORKSHOP
   “LIBERA STAMPA IN LIBERO SOFTWARE”
   :name: workshop-libera-stampa-in-libero-software
   :class: spip

--------------

.. rubric:: PROGRAMMA
   :name: programma
   :class: spip

**SABATO 12 MARZO**

.. rubric:: Ore 9.30
   :name: ore-9.30
   :class: spip

Accoglienza dei partecipanti e presentazione del programma del workshop.

.. rubric:: Ore 10.00
   :name: ore-10.00
   :class: spip

| **Giorgio Beretta**
| Storia e filosofia del software libero.

.. rubric:: Ore 11.00
   :name: ore-11.00
   :class: spip

| **Emanuele Cammarata**
| Cos’è il sistema operativo GNU/Linux: caratteristiche e installazione

.. rubric:: Ore 12.30
   :name: ore-12.30
   :class: spip

Discussione e approfondimenti sugli argomenti trattati.

.. rubric:: Ore 13.00
   :name: ore-13.00
   :class: spip

Pranzo

.. rubric:: Ore 15.00
   :name: ore-15.00
   :class: spip

| **Giuseppe Scatà**
| L’esperienza di U Cuntu, un modo semplice di fare il giornale: “Open
  Office Writer”

.. rubric:: Esercitazione
   :name: esercitazione
   :class: spip

.. rubric:: Ore 16.30
   :name: ore-16.30
   :class: spip

| **Lucio Tomarchio**
| L’esperienza editoriale di avanguardia tecno-politica de “Le
  Siciliane”: un caso di studio

.. rubric:: Ore 17.00
   :name: ore-17.00
   :class: spip

| **Massimo Guglielmino**
| DeskTop Publishing: “Scribus”

--------------

.. rubric:: DOMENICA 13 MARZO
   :name: domenica-13-marzo
   :class: spip

.. rubric:: Ore 10.00
   :name: ore-10.00-1
   :class: spip

| **Pietro Grassi**
| Elaborare le immagini: gimp

.. rubric:: Ore 11.00
   :name: ore-11.00-1
   :class: spip

| **Pietro Grassi**
| Grafica vettoriale: inkscape

.. rubric:: Esercitazione
   :name: esercitazione-1
   :class: spip

.. rubric:: Ore 13.00
   :name: ore-13.00-1
   :class: spip

Pranzo

.. rubric:: Ore 15.00
   :name: ore-15.00-1
   :class: spip

| **Massimo Guglielmino**
| Creazione e gestione di siti web: Wordpress

.. rubric:: Esercitazione
   :name: esercitazione-2
   :class: spip

.. rubric:: Ore 17.00
   :name: ore-17.00-1
   :class: spip

Bilancio del workshop: interventi dei partecipanti e proposte di nuove
iniziative

--------------

| Per iscriversi occorre inviare entro il 7 marzo la `SCHEDA DI
  ADESIONE <http://www.ucuntu.org/pdf/scheda%20adesione_workshop%2012-13%20marzo.doc>`__
  all’indirizzo redazione.lavoriincorso@gmail.com. All’invio della
  richiesta di adesione, si prega di far seguire una telefonata di
  conferma al numero 320/0861081. È richiesto un contributo di 5 €.
| **Il corso svolgerà a Catania in Via Cordai 47. Si consiglia di
  portare un computer portatile. Per informazioni 320/0861081**
| Scarica il `PROGRAMMA
  COMPLETO <http://www.ucuntu.org/pdf/workshop_programma2.pdf>`__
| *Ass. « Lavori in corso » e GNU/Linux User Group di Catania*

 
