Il 04/06/2011, GNU/Linux Installation Party a Valverde
======================================================

:date: 2011-05-15 13:18:56 +0200
:category: Eventi
:tags: GLUG
:slug: art_41_il_04062011_gnulinux_installation_party_a_valverde
:authors: Emanuele
:alias: /content/view/41/1/

I soci del *GNU/Linux User Group di Catania* saranno lieti di incontrare
neo utenti GNU/Linux, simpatizzanti o semplici curiosi che vogliano
provare o abbiano bisogno di consigli sull’installazione di Sistemi
Operativi liberi o sul Software Libero in genere.

.. image:: /images/installationparty_valverde_20110515.jpg

Vi aspettiamo numerosi dalle **ore 15:30** (ora Italiana) del **4 Giugno
2011** in **Via Eremo S. Anna 41 a Valverde (CT)**.

 

**Percorso**: Dalla piazza principale di Valverde andare fino in fondo
al Corso V.Emanuele e svoltare a sinistra per via del Santuario. Seguire
le indicazioni stradali per Eremo S.Anna.
