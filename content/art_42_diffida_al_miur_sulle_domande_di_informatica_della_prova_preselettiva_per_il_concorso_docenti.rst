Diffida al MIUR sulle domande di informatica della prova preselettiva per il concorso docenti
=============================================================================================

:date: 2012-12-13 18:12:54 +0100
:category: Eventi
:tags: GLUG
:slug: art_42_diffida_al_miur_sulle_domande_di_informatica_della_prova_preselettiva_per_il_concorso_docenti
:authors: Ivan Iraci
:alias: /content/view/42/1/

 Il GLug Catania, avendo preso visione delle domande di informatica
della prova preselettiva per il concorso docenti che si terrà da qui a
pochi giorni, non è riuscito a trattenere, oltre ai **conati di vomito
derivanti dall’evidente incompetenza e probabilissima malafede** di chi
ha stilato le domande in questione, l’\ **urgenza di alzare una voce
contro questo scempio e questa vergogna**.

Queste domande infatti, in totale contrasto con le normative vigenti,
danno spazio quasi esclusivo a dettagli specifici di software
proprietari il cui uso, nell’ambito della Pubblica Amministrazione
(quale è la scuola), dovrebbe essere ristretto ai soli casi -
documentati - in cui non esistono alternative libere o aperte. In tal
modo, oltre a non misurare alcuna reale competenza logica e culturale
nell’ambito delle nuove teconologie e della comunicazione digitale, si
opera una vera e propria, oltre che ingiustificata e ingiustificabile,
discriminazione nei confronti dei candidati che usano soltanto software
liberi o aperti.

Questo **orrore** si assomma a quello delle numerosissime domande
**anacronistiche**, **inutilmente nozionistiche**, **ambigue e spesso
errate**.


In tal senso, come associazione in difesa dei diritti digitali e del
libero software, abbiamo trovato unità di intenti con l’azione
intrapresa in questi giorni dall’\ *Associazione per il Sotware Libero*,
sotto la guida di Renzo Davoli, per cui abbiamo deciso di essere
cofirmatari della lettera che è stata inviata al Ministero in data
odierna, di cui segue il testo.

**Firenze, 13 dicembre 2012**

**All’attenzione del Signor Ministro**

**Prof. Francesco Profumo**

**Spettabile**

**MINISTERO PER L’ISTRUZIONE,**

**L’UNIVERSITA’ E LA RICERCA**

**Viale Trastevere, 76/a**

**00153 ROMA**

**        Via PEC agli indirizzi:**

**        uffgabinetto@postacert.istruzione.it**

**        dpit@postacert.istruzione.it**

**        dgpersonalescuola@postacert.istruzione.it**

**        dppr@postacert.istruzione.it**

**        dgrisorseumane@postacert.istruzione.it**

**Oggetto: Diffida, istanza di accesso agli atti e d’intervento nel
procedimento.**

L’\*Associazione per il Software Libero\*, con sede in Firenze, Via
Kyoto n. 8, rappresentata dal sottoscritto Presidente e legale
rappresentante Prof. Renzo Davoli, anche in nome e per conto delle
associazioni indicate nell’allegato elenco che le hanno conferito
espresso mandato, \*espone quanto segue\*.

L’Associazione per il Software Libero e’ una A.P.S., iscritta al
Registrodelle Associazioni della Regione Toscana, che ha come scopo la
promozione dell’uso del software libero e dei formati aperti, in
particolarenell’ambito della Pubblica Amministrazione.

Il TAR del Lazio, con sentenza 428/07, ha riconosciuto il diritto
dell’Associazione per il Software Libero ad agire per la tutela
degliinteressi di cui sopra (si allega copia della sentenza).

 Nei giorni scorsi l’Associazione ha appreso che \ **sul sito del MIUR
sono state pubblicate le domande della prova preselettiva per il
concorso docenti**\
(\ `http://concorsodocenti.miur.it/) <http://concorsodocenti.miur.it/%29>`__\ .

Secondo il bando e il sito, ogni test dovrebbe presentare “7 domande di
competenze digitali”, il cui miglioramento nella popolazione e’ tra
l’altro un pilastro cardine (il numero 6) dell’Agenda Digitale
dell’Unione Europea [2] e su cui la scuola pubblica dovrebbe svolgere un
ruolo determinante per il futuro del paese. Sottolineiamo che si tratta
di verificare  “competenze digitali” e non “conoscenze informatiche
specialistiche”.

Il set di domande in questione risulta essere a tratti fuorviante,
scritto con linguaggio improprio, con numerosi errori. Ma, soprattutto,
il set di domande quasi nulla ha a che fare con la valutazione del grado
di “cultura digitale” dei candidati e comunque \*e’ discriminatorio dal
punto di vista tecnologico ed in particolare nei confronti degli utenti
di software libero\*.

In particolare, delle 490 domande che e’ stato possibile
esaminaresull’esercitatore accessibile al sito
\ http://concorsodocenti.miur.it/\ :

   - 54 presentano risposte errate (almeno 15 di queste contengono
errori gravi);

   - 106 fanno riferimento a dettagli relativi solo ai sistemi operativi
Windows;

Inoltre, nelle domande compaiono:

   - 92 volte la parola Windows,

   - 36 la parola Word,

   - 34 la parola Excel,

   - 12 volte “.doc”.

La citazione di specifici programmi (non di concetti o classi di
applicazioni) discrimina gli utenti che normalmente usano altri
programmi o altri sistemi operativi, fra questi gli utenti di software
libero. Peraltro, quando i software citati sono proprietari, ovvero non
liberi, si ottiene l’effetto di pubblicizzare \*de facto\*, attraverso
il test, questi software. Per fare un’analogia, sarebbe come se nei
test   dell’esame di guida non si facesse riferimento genericamente agli
“autoveicoli”, ma comparisse una domanda come:

“In caso di pioggia occorre aumentare la distanza di sicurezza se si e’
alla guida di una Fiat Cinquecento?”

Vale la pena sottolineare che i prodotti sopra citati (Windows, Word,
Excel), usando la terminologia del comma 1 dell’art. 68 del Codice
dell’amministrazione digitale, Decreto Legislativo 7 marzo 2005, n. 82,
sono “di tipo proprietario” e sottoposti a specifico acquisto di
“licenza d’uso” e quindi non dovrebbero essere acquisiti dalla Pubblica
Amministrazione se non in casi eccezionali.

Si allega l’elenco delle domande con osservazioni e correzioni, anche
disponibili all’URL
\ `http://www.bononia.it/~renzo/elencodomande.commenti.html <http://www.bononia.it/%7Erenzo/elencodomande.commenti.html>`__\

In sintesi, sembra che per gli autori delle domande le “competenze
digitali” si riducano al mero “saper usare un computer”, o meglio, uno
specifico tipo di computer, con uno specifico sistema operativo e
specifiche applicazioni: le domande utili per valutare le reali
“competenze digitali” degli aspiranti insegnanti si contano sulle dita
delle mani. Le “competenze digitali” riguarderanno tutti i docenti della
scuola, anche coloro che non insegneranno materie informatiche o
tecniche, e pertanto ci sembra doveroso insistere sul fatto che questo
test dovrebbe valutare il grado di “cultura digitale” e non delle mere
capacita’ o conoscenze tecniche, per di piu’ su applicazioni specifiche
e \*proprietarie\*.

Le scriventi Associazioni ritengono che i procedimenti amministrativi
che hanno portato all’elaborazione delle suddette domande
discrimineranno gli utenti di software libero e recheranno pregiudizio
agli interessi che le stesse Associazioni rappresentano ed inoltre sono
consapevoli che la realizzazione del concorso con domande cosi’ viziate
aprira’ inevitabilmente alla possibilita’ che, a valle del concorso, ne
venga domandato l’annullamento e pertanto

**INSTANO E DIFFIDANO**

affinche’ Codesta Pubblica Amministrazione voglia:

   - cancellare immediatamente da esso tutte le domande che hanno
risposte errate e/o contengono riferimenti a software e/o a formati
proprietari specifici;

   - fornire copia, ai sensi dell’art 22 e seguenti, L. 241/90,
dell’elenco completo delle domande e risposte di cui sopra e degli atti
tutti relativi ai procedimenti amministrativi che hanno portato
all’elaborazione delle suddette domande e risposte.

Inoltre, sin d’ora

**CHIEDONO** \ ai sensi dell’art. 9 della L. 241/90, di intervenire nel
procedimento \*de quo\* e di interloquire in relazione alle future fasi
di questo.

Riservandosi ogni ulteriore iniziativa, anche ai sensi degli artt. 10 ed
11 L. 241/90, in esito alla disamina degli atti di cui sopra.

Quanto richiesto potra’ essere comunicato al seguente indirizzo PEC:

renzo.davoli@pec.it.

Con ogni osservanza,

Prof. Renzo Davoli

per Associazione per il Software Libero e per le associazioni di cui
all’allegato elenco:

**Primi firmatari**\ :

   - FSFE, Free Software Foundation Italia - Carlo Piana -
\ http://fsfe.org

   - FSUGitalia, Free Software Users Group Italia - Alexjan Carraturo - 
www.fsugitalia.org

   - GFOSS.it , Associazione Italiana per l’Informazione Geografica
Libera

   - Alessandro Furieri - www.gfoss.it

   - ILS - Italian Linux Society - Maurizio Napolitano - www.linux.it

   - LibreItalia (The Document Foundation) - Italo Vignoli -
www.libreoffice.it

   - Wikimedia Italia  - Frieda Brioschi - www.wikimedia.it

 \ **Firmatari:**

   - 2viLUG - Gruppo Utenti GNU/Linux Dueville - Mattia Migliorini -
2vilug@freelists.org

   - Associazione Govonis GNU/LUG - Costantino Pessano -
\ http://www.govonis.org/

   - Associazione LinuxTrent Oltrefersina - Anne Ghisla -
\ http://linuxtrent.it\  <\ http://linuxtrent.nt.it/\ >

   - Associazione RaccattaRAEE - Cristina Carnevali -
\ http://www.ofpcina.net

   - Associazione Prometeo - Francesco Golia -
\ http://www.openlabto.org

   - AVi LUG Alto Vicentino GNU/Linux User Group - Dario Cavedon -
\ `http://www.avilug.it/ <http://www.avilug.it/>`__

   - Banco Informatico del Sud- Domenico Ponte - www.bitsud.it

   - BASLug - Basilicata Linux Users Group - Savino Sasso - 
www.baslug.org

   - Binario Etico Soc.Coop. - Paolo Schettini -
\ http://www.binarioetico.org

   - Community Wiild/wiildos - Matteo Ruffoni - \ http://www.wiild.it

   - Comunita’ Italiana di Ubuntu - Andrea Colangelo - www.ubuntu-it.org

   - eigenLab - Gioacchino Mazzurco - \ http://www.eigenlab.org

   - ERLUG - Emilia Romagna Linux Users Group - Davide Alberani -
\ http://erlug.linux.it/

   - FermoLUG, Associazione Culturale Fermo Linux Users Group - Paolo
Silenzi - www.linuxfm.org

   - Ferrara Linux User Group - Marica Landini -
\ http://ferrara.linux.it/

   - FLOSSmarche - Antonio Esposito - \ http://www.flossmarche.org/

   - FSUGSBT - Simone Vellei - \ http://www.fsugsbt.info/

   - GLUX, Gruppo Lecco Utenti linuX - Carlo Stemberger -
\ http://www.lecco.linux.it/

   - GL-Como, Gruppo Linux Como - De Prisco Davide -
\ http://www.gl-como.it/

   - GLUG Catania - Giorgio Beretta - \ http://catania.linux.it/

   - GLugTo - GNU/Linux user group Torino ONLUS - Rinaldo Graciotti -
www.glugto.org

   - GULLP - Gruppo Utenti Linux Lonate Pozzolo - Alessandro Lorenzi -
www.gullp.net

   - iQuindici - Repubblica democratica dei lettori. Francesco Valotto -
\ http://www.iquindici.org

   - ISF Bologna - Ingegneria Senza Frontiere Bologna - Andrea Mancini -
\ http://isfbologna.wordpress.com/

   - ISF Torino - Ingegneria Senza Frontiere Torino Stefano Cassina -
\ http://isf.polito.it/

   - Libera Informatica - Gianna Papi - www.liberainformatica.it

   - LIFO, Laboratorio di Informatica Free ed Open - Gabriele Biucchi  -
www.lifolab.org

   - LILIS - Laboratorio per l’Informatica Libera Sannita - Giancarlo De
Gregorio – \ http://www.lilis.it

   - LinuxBird Social Magazine - Giovanni Longo - www.linuxbird.org

   - LugAgropoli - Linux User Group Agropoli - Francesco D’Anisi -
\ `http://www.linux.it/~ciccios/lugagropoli.htm <http://www.linux.it/%7Eciccios/lugagropoli.htm>`__

   - LugCR - Nicola Bignami - \ http://www.lugcr.it

   - MontelLUG - Luca Mezzalira - \ http://www.montellug.it

   - Partito Pirata Italia - Athos Gualazzi  - info@partito-pirata.it -
\ http://www.partito-pirata.it

   - PNLUG - LinuxUserGroup di Pordenone - Stefano Morandi -
\ http://pordenone.linux.it

   - RiminiLUG - Andrea Grossi - \ http://www.riminilug.it

   - SLIMP Software Libero Imperia e Provincia - Gianni Rosati -
\ http://www.slimp.it/

   - ViGLug - Francesco Frassinelli - \ http://viglug.org

   - Wireless Community Ninux.org - Saverio Proto -
\ http://www.ninux.org
