Il Nostro GNU/Linux Day 2017
============================

:date: 2017-09-25 01:00:00 +0200
:category: Eventi
:tags: GLUG
:slug: art_46_il_nostro_gnulinux_day_2017
:authors: Antonino Calì
:alias: /content/view/46/1/

**Il Nostro GNULinux Day 2017**

Seguite - se volete - nella vostra piena libertà, le discussioni nella
Mailing List del GLUGCT.

 Potete Iscrivervi - se volete - alla Mailing List **-** Potrete avere
informazioni, suggerimenti,

agevolazioni varie per la conoscenza più appropriata del Sistema
Operativo dei vostri sogni;

fare domande e ottenere, quasi sempre, le risposte più appropriate…basta
non chiedere la luna.

**GNU/Linux - Non è solamente il Sistema Operativo dei vostri sogni,**

**ma anche il Sistema Operativo  della vostra libertà.**     

Qui potete accrescere la vostra conoscenza :
http://www.gnu.org/                 

  e, forse, qui, imparare qualcosa : 
http://www.catania.linux.it/~calianto/SistemaOperativo1.pdf

 

**Per facilitare la vostra iscrizione…**

**basta cliccare qui a sinistra:**

**Mailing List -> lug** - Compilare il Form con il proprio  indirizzo
e-mail e nome.

Completare l’iscrizione cliccando: **Iscrivi**

 Riceverai i messaggi degli iscritti e potrai scrivere alla lista -
**lug@lists.catania.linux.it**

per conoscere ogni evento e partecipare con le tue competenze alla

**divulgazione e alla conoscenza del software libero**,

il solo che può insegnare ad affrancare dai software chiusi

ad ogni libera pratica utilità.

Nino
