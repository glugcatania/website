Documenti e materiale
=====================

:slug: documenti
:date: 2019-05-03 23:20
:tags: Documentazione
:alias: /content/section/3/44/ /content/category/3/30/44/ /component/option,com_docman/Itemid,41/ /component/option,com_docman/task,cat_view/gid,27/Itemid,41/ /component/option,com_docman/task,cat_view/gid,28/Itemid,41/

.. Alias sources:
   /content/section/3/44/ = Documenti
   /content/category/3/30/44/ = Documenti/Guide
   /component/option,com_docman/Itemid,41/ = Materiale
   /component/option,com_docman/task,cat_view/gid,27/Itemid,41/ = Materiale/Eventi
   /component/option,com_docman/task,cat_view/gid,28/Itemid,41/ = Materiale/Eventi/Linux Day 2007

   Not set (yet?) few direct links to the documents:
   /component/option,com_docman/task,doc_download/gid,6/Itemid,41/ = Linux - Carrellata su: COMANDI USI & INSTALLAZIONE
   /component/option,com_docman/task,doc_download/gid,5/Itemid,41/ = statuto
   /component/option,com_docman/task,doc_download/gid,2/Itemid,41/ = Software libero per la didattica (versione estesa)
   /component/option,com_docman/task,doc_download/gid,1/Itemid,41/ = Software libero per la didattica
   /component/option,com_docman/task,doc_download/gid,3/Itemid,41/ = Anonimato in rete


L'associazione
~~~~~~~~~~~~~~

Lo `statuto dell'associazione in formato PDF </documents/statuto.pdf>`_ (pubblicato il 2008-06-18 19:27).

È anche possibile consultare direttamente lo `statuto nella pagina dedicata </statuto/>`_.


Documentazione tecnica
~~~~~~~~~~~~~~~~~~~~~~

.. list-table::
   :header-rows: 1

   * - Titolo
     - Descrizione
     - Pubblicato il
     - Licenza

   * - `SistemaOperativo1.pdf </documents/SistemaOperativo1.pdf>`_
     -  LINUX - Carrellata su: COMANDI USI & INSTALLAZIONE (di Anonino Calì)
     - 2017-08-29 16:16
     - `CC BY-SA 4.0 <http://creativecommons.org/licenses/by-sa/4.0/>`_


Eventi
~~~~~~

Linux Day 2007
--------------

Documentazione prodotta per il LinuxDay 2007 (29/10/2007)

.. list-table::
   :header-rows: 1

   * - Titolo
     - Descrizione
     - Pubblicato il
     - Licenza

   * - `Software libero per la didattica </documents/swlibero_didattica.odp>`_
     - Software libero per la didattica (a cura di Luigi Toscano)
     - 2007-10-28 10:58
     - `CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>`_

   * - `Software libero per la didattica (versione estesa) </documents/swlibero_didattica_esteso.odp>`_
     -  Software libero per la didattica (a cura di Luigi Toscano).

        Versione estesa (lucidi presentati durante la sessione pomeridiana
        presso l'Istituto Regina Elena di Acireale).
     - 2007-10-28 11:00
     - `CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>`_

   * - `Anonimato in rete </documents/AnonimatoInRete.pdf>`_
     - Anonimato in rete (a cura di Giuseppe "uccio" Ferrara)
     - 2007-10-29 19:23
     - `GNU GPL v3.0 <http://www.gnu.org/copyleft/gpl.html>`_

   * - `Introduzione sul software libero </documents/intro_software_libero.pdf>`_
     - Introduzione sulla libertà di diffusione delle idee come diritto naturale dell'uomo,
       ignobilmente redatta da Ivan Iraci.
     - 2007-10-31 09:02
     -
