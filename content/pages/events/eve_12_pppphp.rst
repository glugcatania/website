PPP/PHP
=======

:date: 2008-02-29 13:05:46 +0100
:category: Vita sociale GLUG
:tags: eventi,calendar
:template: event
:slug: eve_12_pppphp
:authors: Administrator
:alias: /component/option,com_events/task,view_detail/agid,12/year,2008/month,01/day,31/Itemid,0/

:event-start: 2008-01-31 08:00
:event-end: 2008-01-31 17:00
:event-location: Piazza Verga - Catania
:summary: Appuntamento alle 21.00 in Piazza Verga, a Catania, per il consueto PPP/PHP.

Appuntamento alle 21.00 in Piazza Verga, a Catania, per il consueto PPP/PHP.
