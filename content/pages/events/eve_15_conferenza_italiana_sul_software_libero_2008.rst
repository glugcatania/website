Conferenza Italiana sul Software Libero 2008
============================================

:date: 2008-04-15 12:24:27 +0200
:category: Eventi
:tags: eventi,calendar
:template: event
:slug: eve_15_conferenza_italiana_sul_software_libero_2008
:authors: Luigi Toscano
:alias: /component/option,com_events/task,view_detail/agid,15/year,2008/month,05/day,16/Itemid,0/

:event-start: 2008-05-16 08:00
:event-end: 2008-05-18 17:00
:event-location: UniversitÃ  di Trento
:summary: Conferenza Italiana sul Software Libero 2008 Trento, 16-17-18 Maggio 2008

Conferenza Italiana sul Software Libero 2008 Trento, 16-17-18 Maggio 2008
