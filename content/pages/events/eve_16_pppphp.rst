PPP/PHP
=======

:date: 2008-06-21 00:37:43 +0200
:category: Vita sociale GLUG
:tags: eventi,calendar
:template: event
:slug: eve_16_pppphp
:authors: Administrator
:alias: /component/option,com_events/task,view_detail/agid,16/year,2008/month,06/day,26/Itemid,0/

:event-start: 2008-06-26 21:00
:event-end: 2008-06-26 01:00
:event-location: Piazza Verga - Catania
:summary: Appuntamento alle 21.00 in Piazza Verga, a Catania, per il consueto PPP/PHP.

Appuntamento alle 21.00 in Piazza Verga, a Catania, per il consueto PPP/PHP.
