Archeologia post-industriale
============================

:date: 2008-08-23 16:20:42 +0200
:category: Eventi
:tags: eventi,calendar
:template: event
:slug: eve_17_archeologia_postindustriale
:authors: Luigi Toscano
:alias: /component/option,com_events/task,view_detail/agid,17/year,2008/month,08/day,28/Itemid,0/

:event-start: 2008-08-28 00:01
:event-end: 2008-08-31 00:01
:event-location: Corso Vittorio Emanuele 4A, Palazzolo Acreide (SR)
:summary: Esposizione interattiva di computer storici funzionanti, software, accessori, documentazione, calcolatrici antiche. Nel corso della manifestazione: Installazioni di GNU/Linux, Dibattiti, Proiezioni, Videogames, e la possibilita’ di usare con le proprie mani computer storici risalenti persino agli anni ’70. 28-31 Agosto 2008, dalle 11:00 alle 12:30 e dalle 16:00 alle 22:00.

Esposizione interattiva di computer storici funzionanti, software, accessori, documentazione, calcolatrici antiche. Nel corso della manifestazione: Installazioni di GNU/Linux, Dibattiti, Proiezioni, Videogames, e la possibilita’ di usare con le proprie mani computer storici risalenti persino agli anni ’70. 28-31 Agosto 2008, dalle 11:00 alle 12:30 e dalle 16:00 alle 22:00.
