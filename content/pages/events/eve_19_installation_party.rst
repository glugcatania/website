Installation Party
==================

:date: 2010-11-04 10:03:07 +0100
:category: Eventi
:tags: eventi,calendar
:template: event
:slug: eve_19_installation_party
:authors: scinziatu
:alias: /component/option,com_events/task,view_detail/agid,19/year,2010/month,11/day,27/Itemid,0/

:event-start: 2010-11-27 15:30
:event-end: 2010-11-27 19:00
:event-location: Valverde, via Eremo S.Anna 41
:summary: Sabato 27 novembre 2010, a Valverde, via Eremo S.Anna 41, nelle immediate vicinanze dell’Eremo, i soci del GNU-Linux User Group Catania saranno lieti di incontrare, a partire dalle ore 15.30,  neo-utenti Linux,simpatizzanti o semplici curiosi, per rispondere alle loro domande su installazione e uso di GNU-Linux e di software libero in genere.

Sabato 27 novembre 2010, a Valverde, via Eremo S.Anna 41, nelle immediate vicinanze dell’Eremo, i soci del GNU-Linux User Group Catania saranno lieti di incontrare, a partire dalle ore 15.30,  neo-utenti Linux,simpatizzanti o semplici curiosi, per rispondere alle loro domande su installazione e uso di GNU-Linux e di software libero in genere.
*Percorso: Dalla piazza principale di Valverde andare fino in fondo al Corso V.Emanuele e svoltare a sinistra per via del Santuario. Seguire le indicazioni stradali per Eremo S.Anna.*
