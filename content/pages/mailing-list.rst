Mailing List
############

:date: 2018-04-08 20:00
:tags: contatti

Al momento sono attive le seguenti mailing list:
  
-  `lug <http://lists.catania.linux.it/cgi-bin/mailman/listinfo/lug>`__:
   La lista 'interna' del lug, dove si disserta su tutto lo scibile
   umano, dai massimi sistemi alla pizza, ma soprattutto del
   nostro/vostro pinguino preferito.

   **ATTENZIONE**: è spesso soggetta a flames e non sempre i nostri toni
   sono pacati come quelli usati negli incontri dal vivo, quindi siine
   consapevole allorquando decidessi di iscriverti! :)
-  `linux <http://lists.catania.linux.it/cgi-bin/mailman/listinfo/linux>`__:
   La lista per i novizi e gli appassionati, dove si scrive solo di
   argomenti tecnici di linux. *Se siete alle prime armi, avete problemi
   con l'installazione, o, in generale, domande tecniche, questa e' la
   lista che fa per voi.*
-  `projects <http://lists.catania.linux.it/cgi-bin/mailman/listinfo/projects>`__:
   La lista di coordinamento per i membri del LUG che partecipano a
   progetti comuni del LUG.

Dalla pagina di gestione di ciascuna mailing list, è possibile accedere
ai relativi archivi (se pubblici).
