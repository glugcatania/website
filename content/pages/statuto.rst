STATUTO
=======

ART. 1
------

È costituita con sede in Catania, Via..............................
l'Associazione culturale di volontariato sociale denominata
"Linux User Group Catania", o brevemente "LUG-Catania". L'Associazione è
democratica, apartitica ed aconfessionale, e non ha fini di lucro.

ART. 2
------

L'associazione si propone di promuovere manifestazioni culturali e scientifiche
per la divulgazione della cultura informatica. In particolare l'associazione si
prefigge di:

- favorire la libera circolazione delle idee, delle conoscenze, e della
  volontarietà nella diffusione delle stesse, con particolare riferimento al
  campo informatico e al software libero, tra cui i sistemi operativi aperti,
  come GNU-Linux, i software con sorgenti liberamente accessibili, studiabili,
  adattabili e perfezionabili da parte dall'utente, in particolare nel mondo
  della scuola, negli enti pubblici, nell'industria ed in ogni altro settore
  in cui esso sia applicabile;
- favorire la libera circolazione delle idee e della conoscenza in campo
  informatico, utilizzando ogni mezzo di comunicazione, anche di massa;
- promuovere lo studio ed il libero utilizzo delle idee e degli algoritmi che
  sottendono al funzionamento dei sistemi informatici;
- promuovere l'applicazione del metodo sperimentale nello studio dei sistemi
  informatici;
- sviluppare studi e ricerche nel settore dell'informatica, conferendo anche
  borse di studio;
- organizzare convegni, manifestazioni e corsi di formazione, sia a livello
  nazionale che internazionale.

Per il raggiungimento dei propri scopi l'associazione potrà istituire
commissioni di lavoro ed organizzare, occasionalmente e nei limiti consentiti
dalla legge, raccolte pubbliche di fondi in concomitanza di celebrazioni o
ricorrenze.
L'Associazione propone di raggiungere i suoi fini attraverso le seguenti
principali attività:

- messa a disposizione presso la propria sede di pubblicazioni scientifiche e
  divulgative, materiale ed attrezzature informatiche di vario tipo, connessione
  ad internet
- organizzazione di convegni e dibattiti
- proposizione di corsi di formazione, sia per i soci, che all'esterno
  dell'associazione stessa
- ogni altra attività, svolta anche in collaborazione con altri enti ed
  associazioni, che rientri negli scopi sociali e possa servire a perseguirli

ART. 3
------

La durata dell'Associazione è stabilita fino al 31 Dicembre 2038.
L'Assemblea potrà prorogare tale durata o consentire anche tacitamente la sua
continuazione a tempo indeterminato.

ART. 4
------

I Soci dell'associazione si distinguono in:

- Soci ordinari: le persone che intendono dare il loro apporto per il
  conseguimento degli scopi associativi, dietro versamento delle quote
  associative, previa domanda di iscrizione in forma scritta;
- Soci studenti: le persone che avendo i requisiti di cui sopra per i soci
  ordinari, non abbiamo ancora compiuto il ventiduesimo anno di età
- Soci simpatizzanti: le persone che partecipano in ogni modo alla vita
  associativa, anche saltuariamente, senza assumersi però alcun obbligo
  organizzativo e senza versare le quote sociali.

Spetta al Consiglio Direttivo deliberare, a maggioranza, sull'ammissione dei
soci. Possono essere ammessi, quali soci studenti, anche i minori che abbiano
compiuto il quattordicesimo anno di età, dietro richiesta scritta dei soggetti
che ne hanno la legale rappresentanza. I soci sono tenuti al pagamento della
quota associativa di Euro 15 annuali per i soci ordinari e per i soci studenti.
L'Assemblea può fissare quote associative diverse.

ART. 5
------

I soci ordinari e studenti hanno diritto di partecipare alle Assemblee, di
assumere cariche sociali, di votare direttamente o per delega per le cariche
sociali, di svolgere il lavoro preventivamente concordato e di recedere
dall'appartenenza all'organizzazione.
Per prendere parte alle iniziative dell'associazione possono essere di volta in
volta deliberate dal Consiglio Direttivo modalità diverse in ragione delle
diverse categorie di appartenenza.
A puro titolo di esempio, possono essere stabilite quote diverse per la
fruizione di corsi, attrezzature, documentazione, in modo che i soci ordinari e
studenti usufruiscano di tariffe più basse dei soci simpatizzanti.
Di volta in volta, particolari iniziative potranno essere riservate all'una o
all'altra categoria di soci.
Solo i soci ordinari o studenti possono accedere ad occasioni di lavoro
derivanti da iniziative dell'associazione.
I soci hanno l'obbligo di rispettare le norme del presente statuto, di pagare
le quote sociali nell'ammontare fissato nel presente Statuto o in quello
diverso stabilito dall'Assemblea. La qualità di socio viene meno in seguito a:

- rinuncia volontaria da comunicare per iscritto al Presidente
- morte o perdita della capacità di agire per le persone fisiche ed estinzione
  per gli enti
- per non aver effettuato il versamento della quota associativa per almeno due
  anni
- per indegnità deliberata dal Consiglio Direttivo, sentito il parere del
  Collegio dei Probiviri

ART. 6
------

Il patrimonio dell'Associazione è costituito da donazioni, lasciti e dalle
eventuali eccedenze di bilancio. Le entrate sono costituite da:

1. contributi degli aderenti
2. contributi di privati
3. contributi dello Stato, di Enti o di istituzioni pubbliche finalizzati
   esclusivamente al sostegno di specifiche e documentate attività o progetti
4. contributi di organismi internazionali
5. donazioni e lasciti testamentari
6. rimborsi derivanti da convenzioni
7. entrate derivanti da attività commerciali e produttive marginali

ART. 7
------

L'esercizio finanziario si chiude al 31 dicembre di ogni anno. Entro il mese di
aprile deve essere convocata l'assemblea per approvare il bilancio consuntivo e
la relazione del Presidente e per determinare eventualmente le quote
associative.
La perdita della qualità di socio per qualsiasi causa non comporta un diritto
sul patrimonio dell'Associazione, nè rimborsi, nè corrispettivi ad alcun
titolo. Alla assemblea il Presidente espone una relazione sull'attività svolta
nell'anno precedente e sull'attività prevista per l'anno in corso.

ART. 8
------

Sono organi dell'Associazione:

- l'Assemblea dei soci;
- il Consiglio Direttivo;
- il Presidente;
- il Collegio dei Revisori dei Conti;
- il Collegio dei Probiviri.

Le cariche sociali sono gratuite salvo il rimborso delle spese vive incontrate
dai componenti degli organi sociali nell'espletamento dei loro incarichi.

ART. 9
------

L'Assemblea dei Soci si riunisce su convocazione del Presidente stesso in via
ordinaria e una volta all'anno.
Spetta all'assemblea deliberare sul bilancio consuntivo, sulla eventuale
determinazione delle quote associative, nominare i componenti del Consiglio
Direttivo di sua competenza, deliberare sulle modifiche dello statuto e sullo
scioglimento dell'Associazione.
L'assemblea si convoca inoltre ogni qualvolta il Presidente ritenga opportuno o
ne sia fatta richiesta da almeno la metà dei soci o da almeno due terzi dei
componenti del Consiglio Direttivo.

ART. 10
-------

L'assemblea delibera validamente con il voto favorevole della maggioranza dei
soci presenti.
È ammesso il voto per delega, conferita ad altro socio per iscritto; ogni socio
non può ricevere più di tre deleghe; il socio studente può ricevere una sola
delega.
Le assemblee sono convocate con avviso contenente l'indicazione del giorno,
dell'ora e del luogo dell'adunanza e l'ordine del giorno, comunicato ad ogni
associato almeno 15 (quindici) giorni prima della data fissata.
L'assemblea può essere convocata anche in luogo diverso dalla sede sociale,
purchè nel territorio della Repubblica Italiana. L'avviso di convocazione può
altresı́ fissare la data della seconda convocazione.
È ammessa la possibilità che le riunioni assembleari si tengano per
teleconferenza o per videoconferenza o attraverso l'uso della rete telematica
Internet, a condizione che tutti i partecipanti possano essere identificati e
sia loro consentito di seguire la discussione e di intervenire in tempo reale
alla trattazione degli argomenti affrontati.
Verificandosi questi requisiti, l'assemblea si considera tenuta nel luogo in
cui si trova il Presidente della riunione e dove pure deve trovarsi il
Segretario della riunione, onde consentire la redazione e la sottoscrizione del
relativo verbale.

ART. 11
-------

Il Consiglio Direttivo è composto da sette membri eletti dall'Assemblea tra i
propri componenti. Il Consiglio Direttivo resterà in carica per 2 anni e i suoi
componenti sono rieleggibili. Il Consiglio Direttivo è investito di tutti i
poteri per lo svolgimento dell'attività sociale e per il raggiungimento degli
scopi associativi, eccetto per le materie riservate alla decisione
dell'assemblea.
Le deliberazioni del Consiglio Direttivo sono prese a maggioranza con la
presenza di almeno la metà dei componenti. In caso di parità dei voti, prevale
il voto del Presidente.
Il Consiglio può delegare determinati compiti in via continuativa al Presidente
ed occasionalmente ad uno o più dei suoi membri può attribuire le funzioni di
delegato per una singola materia specifica. Il Consiglio Direttivo di norma
viene convocato dal Presidente ovvero dai due terzi dei suoi componenti, con
autoconvocazione con le modalità ritenute più idonee.
Qualora, per qualsiasi causa, vengano a mancare uno o più consiglieri, i
rimanenti provvedono a sostituirli e convocano l'Assemblea per la loro
ratifica.
In caso di mancata ratifica si procederà a nuove elezioni.
Qualora venga a mancare, per dimissioni, la maggioranza dei consiglieri,
l'intero Consiglio Direttivo decade dalle sue funzioni e deve procedersi alla
nomina del nuovo Consiglio Direttivo. Nessun compenso è dovuto al membri del
Consiglio.

ART. 12
-------

Vengono eletti dal Consiglio Direttivo, con la maggioranza dei voti dei suoi
componenti, il Presidente il Vicepresidente ed il Segretario. Il Presidente e
in sua assenza o impedimento il vicepresidente convoca e presiede le riunioni
dell'assemblea e del Consiglio Direttivo, vigila sull'attuazione delle loro
deliberazioni, compie gli atti d'urgenza da sottoporre alla ratifica del
consiglio, intrattiene i rapporti con i terzi.
Il Presidente ed il Vice Presidente durano in carica fino alla scadenza o
decadenza del Consiglio Direttivo.

ART. 13
-------

Al Segretario è affidata la gestione contabile dell'associazione. Ha il compito
di tenere aggiornato il Libro Soci e gli altri libri sociali, gestire le
risorse finanziare dell'associazione e collaborare con il Presidente e gli
altri organi sociali per l'amministrazione del patrimonio dell'associazione.

ART. 14
-------

La rappresentanza legale dell'Associazione in giudizio e di fronte ai terzi e
il potere di firma spettano disgiuntamente al Presidente, al vicepresidente
nonchè a coloro che abbiano eventualmente ricevuto dal Consiglio Direttivo
specifici incarichi, ciascuno nell'ambito dei compiti ad essi attribuiti.

ART. 15
-------

Il Presidente è responsabile dell'osservanza di tutte le norme di legge che
regolano tali attività: egli rappresenta l'associazione di fronte ai terzi, in
conformità ai poteri delegati dal Consiglio Direttivo al quale dovrà
relazionare periodicamente l'andamento della sua attività eseguendo le delibere
del Consiglio Direttivo per quanto gli compete.

ART. 16
-------

Il Collegio del Revisori dei conti é composto da tre membri tra cui uno viene
nominato Presidente, eletti, con la maggioranza dei due terzi, per delibera
dell'assemblea dei soci, e durano in carica due anni.
Il Collegio dei Revisori dei Conti controlla l'amministrazione
dell'Associazione, vigila sull'osservanza della legge e dello statuto, accetta
la regolare tenuta delle scritture contabili, certifica la corrispondenza del
bilancio consuntivo alle risultanze dei libri e delle scritture contabili.
Il Collegio dei Revisori dei Conti assiste alle riunioni delle assemblee dei
soci e del Consiglio Direttivo.

ART. 17
-------

Il Collegio dei Probiviri, che dura in carica due anni, è composto da tre soci
eletti singolarmente dall'assemblea a scrutinio segreto. Il Collegio dei
Probiviri è competente a decidere:

- in materia disciplinare su sua iniziativa o del Consiglio direttivo;
- su controversie sull'attuazione e lo svolgimento dell'attività
  dell'associazione, per iniziativa del Consiglio direttivo;
- su controversie di soci con l'associazione;
- su ricorsi di soci inerenti a delibere degli organi dell'Associazione e lo
  svolgimento di riunioni medesimi;
- in materia disciplinare il Collegio dei Probiviri, dopo aver preso in attento
  esame la vicende e comunque disposta l'audizione degli interessati può
  comminare:
- la sospensione dei diritti di socio fino a sei mesi, per fatti rilevanti nei
  comportamenti sociali o in rapporto agli scopi dell'associazione;
- la proposta di decadenza da socio per morosità non sanata nel pagamento della
  quota sociale annua per due anni consecutivi, qualora sia stata deliberata
  dall'Assemblea dei Soci;
- la proposta di esclusione dell'Associazione per grave violazione degli scopi
  sociali o per fatti gravi che abbiano arrecato pregiudizio morale o materiale
  all'Associazione o per aver contravvenuto in modo grave agli scopi
  dell'associazione.

Salvo la facoltà di ricorrere all'autorità giudiziaria nei casi espressivamente
previsti dalla legge, le decisioni dal Collegio dei Probiviri sono definitive.

ART. 18
-------

Lo scioglimento dell'Associazione è deliberato dall'Assemblea dei Soci, con una
maggioranza dei due terzi dei Soci aventi diritto al voto, che provvederà alla
nomina di uno o più liquidatori. L'eventuale patrimonio esistente sarà devoluto
ad un ente o istituzione designati dall'Assemblea dei Soci.

ART. 19
-------

In ragione della natura marcatamente telematica e geograficamente dispersa
dell'associazione, che richiede l'uso prevalente di efficaci mezzi di
comunicazione a distanza, ed allo scopo di favorire l'operatività interna, ai
fini del presente statuto, e salvo ove diversamente specificato, per
"comunicazione scritta" si intende lettera raccomandata, fax, messaggio di
posta elettronica o altro mezzo che comunque consenta l'identificazione certa
del mittente e la conoscibilità del contenuto da parte del destinatario e che
tale risulti definito nel regolamento interno.

ART. 20
-------

In caso di scioglimento, cessazione ovvero estinzione dell'associazione, i beni
che residuano dopo l'esaurimento della liquidazione sono devoluti ad altre
organizzazioni di volontariato operanti in identico o analogo settore o, in
mancanza, secondo le disposizioni del codice civile.
Per quanto non previsto dal presente statuto, si fa riferimento alle vigenti
disposizioni legislative in materia.
