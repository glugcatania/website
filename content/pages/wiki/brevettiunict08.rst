Conferenza sui brevetti @UniCT
##############################

:date: 2008-01-17 18:33
:category: wiki
:alias: /component/option,com_openwiki/Itemid,37/id,brevettiunict08/

Dove e quando
~~~~~~~~~~~~~

Date: 21, 23, 29, 31 gennaio 2008. Differenti argomenti.

Indirizzo di riferimento:
http://www.liaison.unict.it/index.php?option=com_content&task=view&id=44&Itemid=68

Da fare:
''''''''

-  decidere cosa fare esattamente: manifesti, volantini,
   uomini-sandwich, un intervento durante il seminario, e su quale/i
   seminario/i intervenire....
-  agire: pensare agli slogan per i volantini e i manifesti, o trovare
   qualcuno di già pronto, quindi scrivere e stampare.

Materiale
'''''''''

C'è molto materiale interessante in giro. Tentativo di raccolta nella
pagina del `MaterialeBrevettiSoftware <{filename}/pages/wiki/materialebrevettisoftware.rst>`__.

--------------

Commenti
^^^^^^^^

Non so ancora se potro' esserci: ma a quanto pare le giornate cruciali
(quelle in cui si parla di informatica) sono il 21 e il 31. In
particolare il 21 il discorso si apre con il punto principale: quello
della brevettabilita' del software. Penso che e li'che bisogna
intervenire.

Uno spunto:

Il progresso si fonda sulla libera circolazione delle idee. Il vantaggio
temporaneo di pochi non puo'e non deve essere di impedimento ne'di
rallentamento al progresso, che e' vantaggio duraturo e a lungo termine
per tutti. Un esempio e'internet: se fossero stati posti dei limiti alla
riutilizzabilita' in senso evolutivo delle idee, semplicemente oggi la
Rete non sarebbe quella che e': sarebbe per pochi e con pochissime
funzionalita'. Pensiamo ad esempio come sarebbe il mondo se Tim
Berners-Lee avesse brevettato il protocollo html. Lo stesso sito di
questo seminario non sarebbe stato realizzabile: nessuno avrebbe
realizzato Apache, ne' php, ne' mysql, ne' joomla. Pensiamoci bene
quando parliamo di brevetti software: e'davvero questo quello che
vogliamo ?

(stragatto)

Io mi sono registrato e, se ci sarà la possibilità di intervenire nel
dibattito, il mio intervento andrà in questa direzione, magari
sottolineando quelli che ad ora sono i danni derivanti dalla
brevettabilità dei principi attivi farmacologici, tanto che alcune
nazioni (India e Sud Africa, ad esempio) hanno già deciso di ritenere
non validi tali brevetti per rendere producibili a basso costo i
medicinali contro AIDS, malaria e altre malattie che diversamente non
sarebbero state curabili per larga parte della popolazione, ma anche per
"liberare" la ricerca in ambito medico.

(ivan)

Il mio apporto: il software finora è stato considerato come un frutto
dell'ingegno, come una poesia o una canzone; questa cosa poneva al
riparo i produttori da tutta una serie di problemi legali. Se ammettiamo
la brevettabilità, da questa discendono una serie di inevitabili
conseguenze: 1 - non si brevetta una cosa che funziona
approssimativamente, che non funziona sempre e comunque nello stesso
modo e con gli stessi risultati e soprattutto che non crea problemi o
danni derivanti dal suo utilizzo 2 - non esiste software privo di bug 3
- se a causa del tuo programma bacato e brevettato io subisco dei danni,
ti cito ai sensi dell'art.130 e seguenti del codice del consumo (che poi
sarebbe la cosiddetta garanzia) 4 - ora che c'è la class action, il
Codacons partirà alla carica contro qualsiasi produttore di bug
brevettato

interNOT

Infatti: la nota e universalmente adottata clausola di non-garanzia sul
software potrebbe non andare d'accordo con la brevettabilita' e potrebbe
a quel punto diventare clausola nulla. A meno che non vengano
espressamente dichiarate a contratto tutte le condizioni di esclusione
della garanzia (cosa ovviamente impossibile)

(stragatto)
