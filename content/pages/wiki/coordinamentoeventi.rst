Coordinamento eventi
####################

:date: 2009-09-02 12:40
:category: wiki
:alias: /component/option,com_openwiki/Itemid,37/id,coordinamentoeventi/

- `Conferenza sui brevetti @ UNICT <{filename}/pages/wiki/brevettiunict08.rst>`__
- `Giornata sul software libero e la cultura partecipativa
  2008 <{filename}/pages/wiki/giornata_sul_software_libero_e_la_cultura_partecipativa_2008.rst>`__
- `Organizzazione Laboratorio al GAPA e corsi associati <{filename}/pages/wiki/gapa.rst>`__
