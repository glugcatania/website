GAPA
####

:date: 2009-09-03 17:41
:category: wiki
:alias: /component/option,com_openwiki/Itemid,37/id,gapa/

Bozza Proposta
~~~~~~~~~~~~~~

Spett.le Ass. Cult. GAPA In relazione a quanto precedentemente discusso,
l'associazione culturale Linux User Group Catania propone quanto segue:

Fase 1:

#. donazione e installazione di un numero massimo di 6 personal computer configurati con sistema operativo e software libero, predisposti per il collegamento in rete e  per l'accesso a internet con installazione di filtri parentali (la  configurazione potrà essere estesa secondo le esigenze dell'associazione)
#. eventuale formazione di responsabili di laboratorio

Fase2 (In coordinazione con i volontari GAPA):

#. individuazione di software idonei all'utilizzo da parte dei frequentatori del GAPA
#. spiegazione e assistenza su tali software. Possibili argomenti (in base al livello culturale, età, dei possibili fruitori):

   - Internet (motori di ricerca, wikipedia, uso della posta elettronica)
   - Elementi di base su strumenti "da ufficio" (videoscrittura, fogli  di calcolo)
   - Elementi di base su programmi di grafica (GIMP)
   - Software didattici ed educativi (matematica, lingue...)
   - Editoria elettronica
   - Introduzione alla programmazione, per mezzo (p.es.) di linguaggio: LOGO o BASIC

Log veloce della riunione al GAPA del 01/09/09:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ho esposto brevemente la bozza anche a chi non c'era quando abbiamo
parlato del progetto l'ultima volta e le osservazioni venute fuori non
sono moltissime, gli apprezzamenti ci sono stati però (-: .

Organizziamoci
~~~~~~~~~~~~~~

Chi
---

- **Nickname**: disponibile per $ATTIVITAX la mia disponibilità è: $disponibilità
- **nignux** : disponibile per tutte le attività. la
  mia disponibilità è: dal pomeriggio in poi tutta la settimana '''
- **scinziatu**: disponibile per mont.lab. e insegnamento. La mia
  disponibilità è: ogni sera dalle 19.30; sabato tutto il pomeriggio. '''
- **blackout69**: disponibile per mont.lab. La mia disponibilità è:
  qualche sera durante la settimana dalle 19.30; sabato intera giornata.

Riunione
--------

Quando e dove?
sabato 5 settembre presso la sede del G.A.P.A. in via cordai n°47 (traversa di via plebiscito) alle ore 16:30
