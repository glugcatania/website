Giornata sul software libero e la cultura partecipativa 2008
############################################################

:date: 2008-11-04 13:53
:category: wiki
:alias: /component/option,com_openwiki/Itemid,37/id,giornata_sul_software_libero_e_la_cultura_partecipativa_2008/

Bisogna scegliere al più presto **il nome** e **la data** dell'evento,
in modo da mettere in moto la macchina organizzativa... :-)

Le proposte di nome fatte fino ad ora:

- *GNU/Linux: Cultura in Libertà.* (Ivan Iraci)
- *Cultura libera, verso un mondo aperto.* (Pietro Grassi)
- *Free as in Freedom: GNU/Linux Day* (KatolaZ)
- *Libertà informatiche e GNU/Linux* (Scinziatu)
- *GNU Pride o GNU/Linux Pride* (Asbesto ed altri)
- *Piaciri, suGNU Linux!* (Kruznak)

Ne ho sicuramente dimenticate alcune. Integrare, proporre, votare.
