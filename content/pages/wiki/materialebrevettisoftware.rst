Materiale su brevetti software
##############################

:date: 2008-01-18 17:12
:category: wiki
:alias: /component/option,com_openwiki/Itemid,37/id,materialebrevettisoftware/

Tentativo di raccolta del materiale.

Si fa riferimento in alcuni casi alla direttiva (non passata) del 2005,
ma molto materiale resta valido.

Risorse
-------

-  http://www.softwarelibero.it/GNU/nemici/brevetti.shtml
-  http://www.italy.fsfeurope.org/projects/swpat/swpat.it.html
-  http://eupat.ffii.org/
-  http://www.nosoftwarepatents.com/en/m/intro/index.html
-  http://www.interlex.it/copyright/palmieri12.htm
-  http://www.di.unipi.it/~saiu/against-software-patents/ (iniziativa
   chiusa, ma è interessante `questo
   documento <http://www.di.unipi.it/~saiu/against-software-patents/studies_short_en.pdf>`__).
-  http://ciaran.compsoc.com/software-patents.html
-  http://it.openoffice.org/servlets/ReadMsg?list=discussioni&msgNo=4691

Volantini
---------

-  http://www.linux.it/~carlos/swpat/ (non usabili così come sono,
   riferimento ai fatti del 2005)
-  http://patinfo.ffii.org/material/
-  http://www.vrijschrift.org/swpat/art/
