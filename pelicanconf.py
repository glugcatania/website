#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import os

AUTHOR = u'glugct'
SITENAME = u'GNU/Linux User Group Catania'
SITEURL = ''
SITELOGO = u'images/logo_glugct_4_smaller.png'
HIDE_SITENAME = True

FAVICON = u'images/glugct.ico'

CUSTOM_CSS = 'static/css/custom.css'
STATIC_PATHS = [
    'images',
    'static',
    'documents'
]

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = u'it'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


MENUITEMS = (
    ('Collegamenti', '/'),
    ('Contatti', '/contatti/'),
    ('Eventi', '/events-list/'),
    ('Cerca', '/'),
)

GLUGCT_LINKS = (
    ('Menu', [
        ('Home', '/'),
        ('Notizie GLUG', '/'),
        ('Documenti e materiale', '/documenti/'),
        ('Cerca', '/'),
        ('Wiki', '/wiki/'),
        ('Statuto', '/statuto/'),
    ]),
    ('Contatti e progetti', [
        ('Mailing list', '/mailing-list/'),
    ]),
    ('(G)LUG e Hacklab siciliani', [
        ('FreakNet Medialab', 'http://freaknet.org'),
        ('Ennalug', 'http://leonforte.linux.it/'),
        ('LUG Siracusa', 'http://siracusa.linux.it'),
        ('Poetry Hacklab', 'http://poetry.freaknet.org'),
        ('Solira', 'http://www.solira.org'),
        ('VPLUG', 'http://www.vplug.it'),
    ]),
)

# Blogroll

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

DEFAULT_PLUGIN_PATH = os.environ.get(
    'GLUGCT_PELICAN_PLUGIN_PATHS',
    '../pelican-vendored/vendor/plugins'
)

PLUGIN_PATHS = [DEFAULT_PLUGIN_PATH, './plugins']
PLUGINS = ['events', 'i18n_subsites', 'pelican_alias',
           'pelican-page-hierarchy', 'article_filters',
           'video_privacy_enhancer']
PLUGIN_EVENTS = {
    'ics_fname': 'calendar.ics',
}

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}

SIDEBAR_ON_LEFT = True
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_ARTICLE_INFO_ON_INDEX = True
SHOW_ARTICLE_AUTHOR = True
SHOW_ARTICLE_CATEGORY = True
THEME = os.environ.get(
    'GLUGCT_PELICAN_THEME',
    '../pelican-vendored/vendor/themes/pelican-bootstrap3'
)
BOOTSTRAP_THEME = 'lumen'

ARTICLE_URL = 'articoli/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'articoli/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
PATH_METADATA = 'pages/(?P<path>.*)\..*'

ALIAS_DELIMITER = ' '
