"""
The articles_filter plugin adds a Jinja custom_test, is_tagged_as_listable,
that given an article it returns a boolean.
It is intended to be used in Pelican templates like this:
    {% if article is tagged_as_listable %}
        ...
    {% endif %}
"""

from pelican import signals
from . import article_filters

def add_test(pelican):
    """Add is_tagged_as_listable to Pelican."""
    pelican.env.tests.update({'tagged_as_listable': article_filters.is_tagged_as_listable})

def register():
    """Plugin registration."""
signals.generator_init.connect(add_test)
