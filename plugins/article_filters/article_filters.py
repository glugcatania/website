def is_tagged_as_listable(article):
    """Returns true if the article does not contain 'bad' tags"""
    bad_tags = {"calendar", "todo", "hidden"}
    return set(tag for tag in article.tags).isdisjoint(bad_tags)
